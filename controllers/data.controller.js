const {kota,category} = require('../models')
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const Response = require("../helpers/response");

const getAllCity = async (req,res,next)=>{
    const response = new Response(res)
    const {page,row,search} = req.query
    try {
        const options = {
            attributes:['id','nama_kota'],
        }

        if(page) options.offset = page - 1
        if(row) options.limit = row
        if(search) options.where = {
            nama_kota: {[Op.iLike]: `%${search}%`}
        }
        
        const dataKota = await kota.findAll(options)

        return response.Success(response.Ok,dataKota)
        
    } catch (error) {
        next(error)    
    }
}

const getAllCategory = async (req,res,next)=>{
    const response = new Response(res)
    try {
        const options = {
            attributes:['id','category'] 
        }
  
        const data = await category.findAll(options)

        return response.Success(response.Ok,data)
        
    } catch (error) {
        next(error)    
    }
}

module.exports = {
    getAllCity,getAllCategory
}