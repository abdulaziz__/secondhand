const { penawaran, product, account, profile, picture, kota } = require("../models");
const Response = require("../helpers/response");

// menawarkan product
const createPenawaran = async (req, res, next) => {
  const response = new Response(res);
  const { harga } = req.body;
  const { id } = req.params;
  const currentUserId = req.user.id
  try {
    const foundpenawaran = await penawaran.findOne({
      where: {
         id_product:id,
         id_pembeli: currentUserId 
      },
    });

    if(foundpenawaran) throw { message: "anda hanya dapat membuat satu penawaran pada satu product", status: "Bad Request",code: 400}

    const foundProductSeller = await product.findOne({
      where: { id: id },
    });

    if (!foundProductSeller)
      throw {
        message: "product tidak ditemukan",
        status: "Bad Request",
        code: 404,
      };

    const foundProfilePenawar = await profile.findOne({where:{id:currentUserId}})
    if(!foundProfilePenawar.city_id || !foundProfilePenawar.address) throw { message: "lengkapi profile anda terlebih dahulu", status: "Bad Request",code: 400}


    if (foundProductSeller.is_sold)
      throw {
        message: "product sudah terjual",
        status: "Bad Request",
        code: 400,
      };
      
    if (foundProductSeller.account_id === req.user.id) return response.Fail(response.BadRequest,"Hanya Pembeli yang dapat Menawar Barang anda");
      
    const createSellerPenawaran = await penawaran.create({
      id_penjual: foundProductSeller.account_id,
      id_product: foundProductSeller.id,
      id_pembeli: req.user.id,
      harga_tawar: harga,
      is_sold: false,
    });

    const responseData = {
      id_transaction:createSellerPenawaran.id,
      id_produk:id,
      nama_produk:foundProductSeller.nama,
      harga_produk:foundProductSeller.harga,
      harga_tawar:harga,
    }
      

    response.Success(response.Ok, responseData);
    
  } catch (error) {
    next(error)
  }
};

const getPenawaranByCurrentUser = async (req, res, next) => {
  const response = new Response(res);
  const currentUserId = req.user.id
  try {
    const options = {
      attributes: ["id","harga_tawar", "createdAt"],
      include: [
        {
          model: product,
          attributes: ["id","nama", "harga"],
          include:[{
            model: picture,
            attributes:["id","img_url"],
            limit:1
          }]
        },{
          model: account,
          attributes:["id"],
          as:"penawar",
          include: [
            {
              model: profile,
              attributes: ["name","img_url"],
              include: [
                {
                  model: kota,
                  attributes: ["nama_kota"],
                }
              ]
            },
          ],
        },
      ],
      order: [['createdAt','DESC']]
    };

    options.where = { 
      is_sold:false,
      id_penjual: currentUserId 
    };

    const dataPenawaranSeller = await penawaran.findAll(options);
    response.Success(response.Ok, dataPenawaranSeller);
  } catch (error) {
    next(error);
  }
};

const getTerjualByCurrentUser = async (req,res,next) => {
  const response = new Response(res);
  const currentUserId = req.user.id
  try {
    const options = {
      attributes: ["id","harga_tawar", "createdAt"],
      include: [
        {
          model: product,
          attributes: ["id", "nama", "harga"],
          include:[{
            model:picture,
            attributes:["id","img_url"],
            limit:1
          }]
        },{
          model: account,
          attributes:["id"],
          as:"penawar",
          include: [
            {
              model: profile,
              attributes: ["name"],
              include: [
                {
                  model: kota,
                  attributes: ["nama_kota"],
                }
              ]
            },
          ],
        },
      ],
      order: [['createdAt','DESC']]
    };

    options.where = { 
      is_sold:true,
      id_penjual: currentUserId 
    };

    const dataPenawaranSeller = await penawaran.findAll(options);
    response.Success(response.Ok, dataPenawaranSeller);
  } catch (error) {
    next(error);
  }
}

const getPenawaranByIdPenawar = async (req,res,next)=>{
  const { id } = req.params;
  const response = new Response(res);
  const currentUserId = req.user.id
  try {
    const options = {
      attributes: ["id", "harga_tawar", "createdAt", "terima"],
      include: [
        {
          model: product,
          attributes: ["id", "nama", "harga"],
          include:[{
            model:picture,
            attributes:["id","img_url"],
            limit:1
          }]
        },
      ],
      where:{is_sold:false},
      order: [['createdAt','DESC']]
    };

    const penawar = await profile.findOne({
      attributes:["name","img_url"],
      include:[{
        model:kota,
        attributes:["nama_kota"]
      }],
      where:{id}
    })

    if(!penawar) throw { message: "penawar tidak ditemukan", status: "Bad Request",code: 404}

    options.where = { id_pembeli:id, id_penjual:currentUserId, is_sold:false };
    const dataPenawaranSeller = await penawaran.findAll(options);

    if(dataPenawaranSeller.length === 0) return response.Fail(response.NotFound,"not found", "tidak terdapat penawaran dari user terhadap produk anda");

    const responseData = {
      "penawar":penawar.name,
      "kota":penawar.kotum.nama_kota,
      "profile_url":penawar.img_url,
      "daftarPenawaran" : dataPenawaranSeller
    }

    response.Success(response.Ok, responseData);
  } catch (error) {
    next(error);
  }
}

const getPenawaranById = async (req,res,next)=>{
  const { id } = req.params;
  const response = new Response(res);
  const currentUserId = req.user.id
  try {
    const options = {
      attributes: ["id", "harga_tawar", "createdAt", "terima", "id_pembeli"],
      include: [
        {
          model: product,
          attributes: ["id", "nama", "harga"],
          include:[{
            model:picture,
            attributes:["id","img_url"],
            limit:1
          }]
        },
      ],
      where:{is_sold:false}
    };

    options.where = { id:id, id_penjual:currentUserId, is_sold:false };
    const dataPenawaranSeller = await penawaran.findOne(options);

    if(!dataPenawaranSeller) return response.Fail(response.NotFound,"not found", `tidak terdapat penawaran dengan id ${id} terhadap produk anda`);

    const penawar = await profile.findOne({
      attributes:["name","img_url"],
      include:[{
        model:kota,
        attributes:["nama_kota"]
      }],
      where:{id:dataPenawaranSeller.id_pembeli}
    })

    if(!penawar) throw { message: "penawar tidak ditemukan", status: "Bad Request",code: 404}

    const responseData = {
      "penawar":penawar.name,
      "kota":penawar.kotum.nama_kota,
      "profile_url":penawar.img_url,
      "penawaran" : dataPenawaranSeller
    }

    response.Success(response.Ok, responseData);
  } catch (error) {
    next(error);
  }
}

const terimaPenawaranById = async (req,res,next)=>{
  const { opsi } = req.body;
  const { id } = req.params;
  const idSeller = req.user.id;
  const response = new Response(res);
  try {
    // mencari id Penawaran
    const foundpenawaran = await penawaran.findOne({where: { id: id }});

    if(!foundpenawaran) throw { message: "transaction tidak ditemukan", status: "Bad Request",code: 404}

    if(foundpenawaran.id_penjual !== idSeller) throw {
      message: "Hanya Seller yang dapat Merubah Data",
      status: "Bad Request",
      code: 400,
    };

    const foundSellerProduct = await product.findOne({where: { id:foundpenawaran.id_product }});
 
    if(!foundSellerProduct) throw { message: "product tidak ditemukan", status: "Bad Request",code: 404}

    if (!opsi) {
      await penawaran.destroy({where: { id }});
      response.Success(response.Ok,"Penawaran berhasil ditolak, dan dihapus")
    } else {
      await penawaran.update({terima: true,},{where:{id}})
      response.Success(response.Ok,"Penawaran berhasil diterima")
    }

  } catch (error) {
    next(error);
  }
}

const editStatusPenawaran = async (req, res, next) => {
  const { opsi } = req.body;
  const { id } = req.params;
  const idSeller = req.user.id;
  const response = new Response(res);
  try {
    // mencari id Penawaran
    const foundpenawaran = await penawaran.findOne({
      where: { id: id },
    });

    if(!foundpenawaran) throw { message: "transaction tidak ditemukan", status: "Bad Request",code: 404}

    if(foundpenawaran.id_penjual !== idSeller) throw {
      message: "Hanya Seller yang dapat Merubah Data",
      status: "Bad Request",
      code: 400,
    };

    const foundSellerProduct = await product.findOne({
      where: { id:foundpenawaran.id_product },
    });
 
    if(!foundSellerProduct) throw { message: "product tidak ditemukan", status: "Bad Request",code: 404}

    if (opsi) {

      const updatedStatusPenawaran = await penawaran.update(
        {
          is_sold: true,
        },
        {
          where: { id },
        }
      );

      await product.update({
        is_sold: true,
      },{
        where: { id:foundpenawaran.id_product }
      });

      if (updatedStatusPenawaran)
        await penawaran.destroy({
          where: { id_product: foundSellerProduct.id, is_sold: false },
        });
        
      response.Success(
        response.Ok,
        "Berhasil di ganti dan menghapus penawaran lain"
      );
    }
    else  {
      const deleteData = await penawaran.destroy({
        where: { id },
      });
      response.Success(response.Ok, "berhasil di hapus");
    }
  } catch (error) {
    next(error);
  }
};

const deletePenawaranById = async (req, res, next) => {
  try {
    const response = new Response(res);
    // ambil id Endpoint
    const { id } = req.params;
    // ambil id user login
    const current_user = req.user.id;

    //mencari id penawaran
    const findPenawaran = await penawaran.findOne({
      where: {
        id: id,
      },
    });
    // jika id penawaran tidak di temukan
    if (!findPenawaran)
      throw {
        message: "id penawaran tidak ditemukan",
        status: "Bad Request",
        code: 404,
      };
    // mencari id Pembeli
 
    if (findPenawaran.id_pembeli !== current_user)
      throw {
        message: "anda tidak punya hak untuk menghapus transaction",
        status: "Bad Request",
        code: 400,
      };

    // delete penawaran sesuai id
    const deletePenawaran = await penawaran.destroy({
      where: {
        id: id,
      },
    });
    
    response.Success(response.Ok, "berhasil di hapus");
  } catch (error) {
    next(error);
  }
};

module.exports = { createPenawaran, getPenawaranByCurrentUser, getTerjualByCurrentUser ,getPenawaranByIdPenawar, editStatusPenawaran, deletePenawaranById, terimaPenawaranById, getPenawaranById };
