const {product,penawaran,picture} = require('../models')
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const Response = require("../helpers/response");

const getSellerNotification = async (req,res,next)=>{
    const response = new Response(res)
    const account_id = req.user.id
    try {
        const penawaranList = await penawaran.findAll({
            attributes:["id","harga_tawar","createdAt"],
            include:[{
                model:product,
                attributes:["id","nama","harga"],
                include:[{
                    model:picture,
                    attributes:["img_url"],
                    limit:1
                }]
            }],
            where:{id_penjual:account_id, is_sold:false}
        })

        const terjualList = await penawaran.findAll({
            attributes:["id","harga_tawar","createdAt"],
            include:[{
                model:product,
                attributes:["id","nama","harga"],
                include:[{
                    model:picture,
                    attributes:["img_url"],
                    limit:1
                }]
            }],
            where:{id_penjual:account_id, is_sold:true}
        })

        const responseData = {
            produk_terjual : terjualList,
            penawaran_masuk:penawaranList
        }
         
        return response.Success(response.Ok,responseData)
        
    } catch (error) {
        next(error)    
    }
}

const getProductNotification = async(req,res,next)=>{
    const response = new Response(res)
    const account_id = req.user.id
    try {
        const newProductList = await product.findAll({
            attributes:["id","nama","harga","createdAt"],
            include:[{
                model:picture,
                attributes:["img_url"],
                limit:1
            }],
            where:{account_id}
        })
        
        return response.Success(response.Ok,newProductList)
        
    } catch (error) {
        next(error)    
    }
}

const getBuyerNotification = async(req,res,next)=>{
    const response = new Response(res)
    const account_id = req.user.id
    try {
        const penawaranList = await penawaran.findAll({
            attributes:["id","harga_tawar","terima","createdAt"],
            include:[{
                model:product,
                attributes:["id","nama","harga"],
                include:[{
                    model:picture,
                    attributes:["img_url"],
                    limit:1
                }]
            }],
            where:{id_pembeli:account_id,terima:null}
        })

        const pembelianList = await penawaran.findAll({
            attributes:["id","harga_tawar","terima","createdAt"],
            include:[{
                model:product,
                attributes:["id","nama","harga"],
                include:[{
                    model:picture,
                    attributes:["img_url"],
                    limit:1
                }]
            }],
            where:{id_pembeli:account_id,is_sold:true}
        })

        const penawaranDiterimaList = await penawaran.findAll({
            attributes:["id","harga_tawar","terima","createdAt"],
            include:[{
                model:product,
                attributes:["id","nama","harga"],
                include:[{
                    model:picture,
                    attributes:["img_url"],
                    limit:1
                }]
            }],
            where:{id_pembeli:account_id,terima:true}
        })
        const responseData = {
            produk_dibeli:pembelianList,
            penawaran_diterbitkan:penawaranList,
            penawaran_sudah_diterima:penawaranDiterimaList
        }
        return response.Success(response.Ok,responseData)
        
    } catch (error) {
        next(error)    
    }
}



module.exports = {
    getSellerNotification,getProductNotification,getBuyerNotification
}