const {
  account,
  profile,
  product,
  category,
  kota,
  picture,
  penawaran,
} = require("../models");
const Response = require("../helpers/response");
const Sequelize = require("sequelize");
const { deleteImage, productUpload } = require("../misc/cloudinary");
const Op = Sequelize.Op;

module.exports = {
  getAll: async (req, res, next) => {
    const response = new Response(res);
    const { page, row, search } = req.query;
    try {
      const options = {
        attributes: { exclude: ["updatedAt", "is_sold"] },
        include: [
          {
            model: category,
            attributes: ["category"],
          },
          {
            model: account,
            attributes: ["id"],
            include: [
              {
                model: profile,
                attributes: ["name"],
                include: [
                  {
                    model: kota,
                    attributes: ["nama_kota"],
                  },
                ],
              },
            ],
          },
          {
            model: picture,
            attributes: ["img_url"],
            limit:1
          },
        ],
        order: [['createdAt','DESC']],
        where:{
          is_sold:false,
          published:true
        }
      };

      if (page) options.offset = page - 1;
      if (row) options.limit = row;
      if (search)
        options.where = {
          nama: { [Op.iLike]: `%${search}%` },
        };

      const dataProduct = await product.findAll(options);

      if (dataProduct) {
        const dataProductMapped = dataProduct.map((eachProduct) => {
          eachProduct.dataValues.category =
            eachProduct.dataValues.category.dataValues.category;
          eachProduct.dataValues.seller_name =
            eachProduct.dataValues.account.dataValues.profile.dataValues.name;
          eachProduct.dataValues.seller_kota =
            eachProduct.dataValues.account.dataValues.profile.dataValues.kotum.dataValues.nama_kota;

          let hasUrl = eachProduct.dataValues.pictures.length > 0;
          eachProduct.dataValues.img_url = hasUrl
            ? eachProduct.dataValues.pictures[0].dataValues.img_url
            : null;

          delete eachProduct.dataValues.account;
          delete eachProduct.dataValues.pictures;

          return eachProduct;
        });

        return response.Success(response.Ok, dataProductMapped);
      }
    } catch (error) {
      next(error);
    }
  },

  getByCategory: async (req, res, next) => {
    const response = new Response(res);
    const { page, row } = req.query;
    const category_name = req.params.category;
    try {
      const options = {
        attributes: { exclude: ["updatedAt", "is_sold"] },
        include: [
          {
            model: category,
            attributes: ["category"],
          },
          {
            model: account,
            attributes: ["id"],
            include: [
              {
                model: profile,
                attributes: ["name"],
                include: [
                  {
                    model: kota,
                    attributes: ["nama_kota"],
                  },
                ],
              },
            ],
          },
          {
            model: picture,
            attributes: ["img_url"],
            limit: 1,
          },
        ],
        order: [['createdAt','DESC']]
      };

      const findCategory = await category.findOne({
        where: { category: category_name },
      });

      if (!findCategory)
        throw { message: "Category not found", status: "Not Found", code: 404 };

      if (page) options.offset = page - 1;
      if (row) options.limit = row;

      options.where = { kategori_id: findCategory.id ,is_sold:false, published:true };

      const dataProduct = await product.findAll(options);

      if (dataProduct) {
        const dataProductMapped = dataProduct.map((eachProduct) => {
          eachProduct.dataValues.category =
            eachProduct.dataValues.category.dataValues.category;
          eachProduct.dataValues.seller_name =
            eachProduct.dataValues.account.dataValues.profile.dataValues.name;
          eachProduct.dataValues.seller_kota =
            eachProduct.dataValues.account.dataValues.profile.dataValues.kotum.dataValues.nama_kota;

          delete eachProduct.dataValues.account;

          let hasUrl = eachProduct.dataValues.pictures.length > 0;
          eachProduct.dataValues.img_url = hasUrl
            ? eachProduct.dataValues.pictures[0].dataValues.img_url
            : null;

          delete eachProduct.dataValues.pictures;

          return eachProduct;
        });

        return response.Success(response.Ok, dataProductMapped);
      }
    } catch (error) {
      next(error);
    }
  },

  getById: async (req, res, next) => {
    const response = new Response(res);
    const id = req.params.id;
    const current_id = req.user.id
    try {
      const options = {
        attributes: { exclude: ["updatedAt", "is_sold"] },
        include: [
          {
            model: category,
            attributes: ["category"],
          },
          {
            model: account,
            attributes: ["id"],
            include: [
              {
                model: profile,
                attributes: ["name","img_url"],
                include: [
                  {
                    model: kota,
                    attributes: ["nama_kota"],
                  },
                ],
              },
            ],
          },
          {
            model: picture,
            attributes: ["id", "img_url"],
          },
        ],
      };

      options.where = { id };

      const dataProduct = await product.findOne(options);

      if (!dataProduct) throw { message: "product not found", status: "Not Found", code: 404 };
  
      if(dataProduct.account_id !== current_id) {
        const found_penawaran = await penawaran.findOne({where:{id_product:id,id_pembeli:current_id}})
        const statusPenawaran = found_penawaran ? false:true
        dataProduct.dataValues.dapatMenawar = statusPenawaran
      }else{
        dataProduct.dataValues.dapatMenawar = false
      }

      dataProduct.dataValues.category =
        dataProduct.dataValues.category.dataValues.category;
      dataProduct.dataValues.seller_name =
        dataProduct.dataValues.account.dataValues.profile.dataValues.name;
      dataProduct.dataValues.seller_img_url =
        dataProduct.dataValues.account.dataValues.profile.dataValues.img_url;
      dataProduct.dataValues.seller_kota =
        dataProduct.dataValues.account.dataValues.profile.dataValues.kotum.dataValues.nama_kota;

      let hasUrl = dataProduct.dataValues.pictures.length > 0;
      dataProduct.dataValues.img_url = hasUrl
          ? dataProduct.dataValues.pictures
          : null;
      delete dataProduct.dataValues.account;
      delete dataProduct.dataValues.pictures;

      return response.Success(response.Ok, dataProduct);
    } catch (error) {
      next(error);
    }
  },

  getByCurrentUser: async (req, res, next) => {
    const response = new Response(res);
    const currentUserId = req.user.id;
    try {
      const findUser = await account.findOne({ where: { id: currentUserId } });
      if (!findUser)
        throw { message: "user not found", status: "Not Found", code: 404 };

      const options = {
        attributes: { exclude: ["updatedAt", "is_sold"] },
        include: [
          {
            model: category,
            attributes: ["category"],
          },
          {
            model: picture,
            attributes: ["img_url"],
            limit: 1,
          },
        ],
        order: [['createdAt','DESC']]
      };

      options.where = { account_id: currentUserId,is_sold:false };

      const dataProduct = await product.findAll(options);

      if (dataProduct) {
        const dataProductMapped = dataProduct.map((eachProduct) => {
          eachProduct.dataValues.category =
            eachProduct.dataValues.category.dataValues.category;
          let hasUrl = eachProduct.dataValues.pictures.length > 0;
          eachProduct.dataValues.img_url = hasUrl
            ? eachProduct.dataValues.pictures[0].dataValues.img_url
            : null;

          delete eachProduct.dataValues.pictures;
          return eachProduct;
        });

        return response.Success(response.Ok, dataProductMapped);
      }
    } catch (error) {
      next(error);
    }
  },

  create: async (req, res, next) => {
    const response = new Response(res);
    const { nama, harga, kategori_id, deskripsi } = req.body;
    const user_id = req.user.id;
    const images = req.files;
    try {
      const findUser = await account.findOne({ where: { id: user_id } });
      if (!findUser) throw { message: "user not found", status: "Not Found", code: 404 };

      const foundProfile = await profile.findOne({where:{ id: user_id }})
      if(!foundProfile.city_id || !foundProfile.address) throw { message: "lengkapi profile terlebih dahulu", status: "bad Request", code: 400 };

      const findCategory = await category.findOne({
        where: { id: kategori_id },
      });

      if (!findCategory)
        throw { message: "category not found", status: "Not Found", code: 404 };

      const newProduct = await product.create({
        nama,
        harga,
        kategori_id,
        deskripsi,
        account_id: user_id,
      });

      if (newProduct && images) {
        const product_id = newProduct.id;

        const imagesMapped = images.map((eachImage) => {
          return {
            path: eachImage.path,
            name: eachImage.filename,
          };
        });

        imagesMapped.forEach(async (image) => {
          let img_url = await productUpload(image, product_id);
          await picture.create({ product_id, img_url });
        });

        const responseData = {
          id: product_id,
          nama: newProduct.nama,
          harga: newProduct.harga,
        };

        return response.Success(response.Created, responseData);
      }
    } catch (error) {
      next(error);
    }
  },

  publishById: async (req,res,next) =>{
    const response = new Response(res);
    const { published } = req.body;
    const id = req.params.id;
    const user_id = req.user.id;
    try {
      if(typeof published !== "boolean") throw { message: "published harus bertipe boolean", status: "Bad Request", code: 400 };

      const foundUser = await account.findOne({ where: { id: user_id } });
      if (!foundUser)throw { message: "user not found", status: "Not Found", code: 404 };

      const foundProduct = await product.findOne({ where: { id } });
      if (!foundProduct)throw { message: "product not found", status: "Not Found", code: 404 };

      const validUserToUpdate = foundProduct.account_id === user_id;
      if (!validUserToUpdate)
        throw {
          message: "you dont have access to update",
          status: "Forbidden",
          code: 403,
        };

      const updatedProduct = await product.update({published},{ where: { id } });
      if(updatedProduct) return response.Success(response.Ok, "produk berhasil diupdate");

    } catch (error) {
      next(error);
    }

  },

  update: async (req, res, next) => {
    const response = new Response(res);
    const { nama, harga, kategori_id, deskripsi } = req.body;
    const id = req.params.id;
    const user_id = req.user.id;
    const images = req.files;
    try {
      const foundUser = await account.findOne({ where: { id: user_id } });
      if (!foundUser)
        throw { message: "user not found", status: "Not Found", code: 404 };

      const foundProduct = await product.findOne({ where: { id } });
      if (!foundProduct)
        throw { message: "product not found", status: "Not Found", code: 404 };

      const validUserToUpdate = foundProduct.account_id === user_id;
      if (!validUserToUpdate)
        throw {
          message: "you dont have access to update",
          status: "Forbidden",
          code: 403,
        };

      const findCategory = await category.findOne({
        where: { id: kategori_id },
      });
      
      if (!findCategory)
        throw { message: "category not found", status: "Not Found", code: 404 };

      const updatedProduct = await product.update(
        {
          nama,
          harga,
          kategori_id,
          deskripsi,
          account_id: user_id,
        },
        { where: { id } }
      );

      if (updatedProduct && images.length) {    
        const product_id = id;
        
        const imagelist= await picture.findAll({where:{product_id}})
        imagelist.forEach(eachImage=>{
          deleteImage(eachImage.img_url).then(result=>result)
        })

        const imagesMapped = images.map((eachImage) => {
          return {
            path: eachImage.path,
            name: eachImage.filename,
          }});


        await picture.destroy({ where: { product_id } });

        imagesMapped.forEach(async (image) => {
          let img_url = await productUpload(image, product_id);
          await picture.create({ product_id, img_url });
        });
    }
      return response.Success(response.Ok, "produk berhasil diupdate");
    } catch (error) {
      next(error);
    }
  },

  deleteById: async (req, res, next) => {
    const response = new Response(res);
    const id = req.params.id;
    const user_id = req.user.id;
    try {
      const foundUser = await account.findOne({ where: { id: user_id } });
      if (!foundUser)
        throw { message: "user not found", status: "Not Found", code: 404 };

      const foundProduct = await product.findOne({ where: { id } });
      if (!foundProduct)
        throw { message: "product not found", status: "Not Found", code: 404 };

      const validUserToUpdate = foundProduct.account_id === user_id;
      if (!validUserToUpdate)
        throw {
          message: "you dont have access to delete",
          status: "Forbidden",
          code: 403,
        };

      const deletedProduct = await product.destroy({ where: { id } });

      const images = await picture.findAll({ where: { product_id: id } });

      images.forEach(async (eachImage) => {
        await deleteImage(eachImage.img_url);
      });

      await picture.destroy({ where: { product_id: id } });

      if (deletedProduct)
        return response.Success(response.Ok, "produk berhasil didelete");
    } catch (error) {
      next(error);
    }
  },

  deleteImageById: async (req, res, next) => {
    const response = new Response(res);
    const id = req.params.id;
    const user_id = req.user.id;
    try {
      const foundUser = await account.findOne({ where: { id: user_id } });
      if (!foundUser)
        throw { message: "user not found", status: "Not Found", code: 404 };

      const foundImage = await picture.findOne({ where: { id } });
      if (!foundImage)
        throw { message: "image not found", status: "Not Found", code: 404 };

      const product_id = foundImage.product_id;

      const foundProduct = await product.findOne({ where: { id: product_id } });
      if (!foundProduct)
        throw { message: "product not found", status: "Not Found", code: 404 };

      const validUserToDelete = foundProduct.account_id === user_id;
      if (!validUserToDelete)
        throw {
          message: "you dont have access to delete",
          status: "Forbidden",
          code: 403,
        };

      const deletedImage = await picture.destroy({ where: { id } });
      await deleteImage(foundImage.img_url);

      if (deletedImage)
        return response.Success(response.Ok, "image berhasil didelete");
    } catch (error) {
      next(error);
    }
  },
};
