const multer = require('multer')

const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, `${req.user.id}-product-${file.originalname.split('.')[0]}`)
  }
})

const uploadPhoto = multer({ 
  storage: storage,
  limits: { fileSize: 25e+7 },
  fileFilter: (req, file, cb) => {  
    const validImageExtension = ['image/png','image/jpeg','image/jpg']
    if (validImageExtension.includes(file.mimetype)) cb(null, true);
    else cb("invalid file extension", false)
  }
}) 


module.exports = uploadPhoto