const { JsonWebTokenError } = require('jsonwebtoken');
const request = require('supertest');
const { response } = require('../app');
const app = require('../app');

const newToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwibmFtZSI6InVzZXJuYW1lIGR1YSIsImlhdCI6MTY1ODM3NzMyMiwiZXhwIjoxNjU4Mzg0NTIyfQ.JcLSFXI3-_bq6pv2qMeh_FUhOf7fuMEz_LTw1OLOnRQ';
const expiredToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6InVzZXJuYW1lIHNhdHUiLCJpYXQiOjE2NTc1OTkxMTYsImV4cCI6MTY1NzYwNjMxNn0.0xyNOfjNqRJKlT396svzJnwvq1FxL2PYn3cdv1rB2dc';
const invalidToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6InVzZXJuYW1lIHNhdHUiLCJpYXQiOjE2NTc2MzA1NDMsImV4cCI6MTY1NzYzNzc0M30.QoqnKgloggZJgtkL1PByy';



// error route
describe('test error router', () => {
    if ('should return not found endpoint', async () => {
        const res = await request(app).get('/');
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('Endpoint not found');
    });
})

// //REGIST
describe('POST USER',()=>{
    it('should create new user', async () => {
        const res = await request(app).post('/index/register').send({
            'email'   : 'sauvagx@gmail.com',
            'password': 'sauvagx123',
            'name'    : 'mama'
        });
        const { status } = res.body;
        expect(res.status).toBe(201);
        expect(res.body).toHaveProperty('data');
        expect(status).toBe('Created');
    });
    it('Fail create user', async () => {
        const res = await request(app).post('/index/register').send({
            'email'   : 'aziz@gmail.com',
            'password': 'hai123',
            'name'    : 'hai test'
        });
        const { status, message } = res.body;
        expect(res.status).toBe(400);
        expect(status).toBe('Bad Request');
        expect(message).toBe('email address is already in used');
    });
});

// //LOGIN
describe('POST USER',()=>{
    it('Should be check user login', async () => {
        const res = await request(app).post('/index/login').send({
            'email'   : 'aziz@gmail.com',
            'password': 'hai123'
        });
        const { status } = res.body;
        expect(res.status).toBe(200);
        expect(status).toBe('Login Success');
    });
    it('Incorrect email', async () => {
        const res = await request(app).post('/index/login').send({
            'email'   : 'ziza@gmail.com',
            'password': 'hai123'
        });
        const { status, message } = res.body;
        expect(res.status).toBe(400);
        expect(status).toBe('Bad Request');
        expect(message).toBe('email is incorrect');
    });
    it('Incorrect password', async () => {
        const res = await request(app).post('/index/login').send({
            'email'   : 'aziz@gmail.com',
            'password': 'hia123'
        });
        const { status, message } = res.body;
        expect(res.status).toBe(400);
        expect(status).toBe('Bad Request');
        expect(message).toBe('password is incorrect');
    });
});

// GET PROFILE
describe('GET USER PROFILE',()=>{
    it('Suppose to get user profile', async () => {
        const res = await request(app).get('/api/profile').set('Authorization', `Bearer ${newToken}`)
        expect(res.status).toBe(200);
    });
    it('Failed to get user profile', async () => {
        const res = await request(app).get('/api/profile').set('Authorization', `Bearer ${expiredToken}`)
        const { status } = res.body;
        expect(res.status).toBe(400);
        expect(status).toBe('jwt expired');
    });
});

//UPDATE PROFILE
describe('UPDATE USER PROFILE',()=>{
    it('Update user profile', async () => {
        const res = await request(app).put('/api/profile').send({
            'name': 'aziz',
            'phone_number': '08123456789',
            'address': 'jalan abc',
            'city_id': 1,
            'kota': 'MANOKWARI SELATAN'
        })
        .set('Authorization', `Bearer ${newToken}`)
        const { data } = res.body;
        expect(res.status).toBe(200);
        expect(data).toBe('profile berhasil diupdate');
    });
    it('fail auth update user profile', async () => {
        const res = await request(app).get('/api/profile').send({
            'name': 'username satu',
            'phone_number': '08123456789',
            'address': 'jalan abc',
            'city_id': 1,
            'kota': 'MANOKWARI SELATAN'
        })
        .set('Authorization', `Bearer ${expiredToken}`)
        const { status } = res.body;
        expect(res.status).toBe(400);
        expect(status).toBe('jwt expired');
    });
    it('invalid auth update user profile', async () => {
        const res = await request(app).get('/api/profile').send({
            'name': 'username satu',
            'phone_number': '08123456789',
            'address': 'jalan abc',
            'city_id': 1,
            'kota': 'MANOKWARI SELATAN'
        })
        .set('Authorization', `Bearer ${invalidToken}`)
        const { status } = res.body;
        expect(res.status).toBe(400);
        expect(status).toBe('invalid signature');
    });
});

//GET DATA
describe('GET DATA',()=>{
    it('Should get city name', async () => {
        const res = await request(app).get('/api/data/kota?search=bek')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data} = res.body
        expect(code).toBe(200);
        expect(data[0].id).toBe(413);
        expect(data[0].nama_kota).toBe('BEKASI');
        
    });
    it('Show city list on page 10', async () => {
        const res = await request(app).get('/api/data/kota').send({
            'page': 10
        })
        .set('Authorization', `Bearer ${newToken}`)
        const { code } = res.body
        expect(code).toBe(200);
    });
    it('Show Category', async () => {
        const res = await request(app).get('/api/data/kategori')
        .set('Authorization', `Bearer ${newToken}`)
        const { code } = res.body
        expect(code).toBe(200);
    });
    it('Show all product', async () => {
        const res = await request(app).get('/api/product')
        .set('Authorization', `Bearer ${newToken}`)
        const { code } = res.body
        expect(code).toBe(200);
    });
    it('Show product category', async () => {
        const res = await request(app).get('/api/product/category/elektronik')
        .set('Authorization', `Bearer ${newToken}`)
        const { code } = res.body
        expect(code).toBe(200);
    });
    it('Show product by id', async () => {
        const res = await request(app).get('/api/product/id/1')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data} = res.body
        expect(code).toBe(200);
        expect(data.nama).toBe("jam tangan");
        expect(data.seller_name).toBe("aziz");
    });
    it('Should not show product by id', async () => {
        const res = await request(app).get('/api/product/id/10')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, status, message } = res.body
        expect(code).toBe(404);
        expect(status).toBe("Not Found");
        expect(message).toBe("product not found");
    });
    it('Show product by user login', async () => {
        const res = await request(app).get('/api/product/myproduct')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body
        expect(code).toBe(200);
        expect(data[0].id).toBe(4);
        expect(data[0].nama).toBe("samsung galaxy s7");
        expect(data[0].harga).toBe(950000,);
    });
    it('Should not show product by user login', async () => {
        const res = await request(app).get('/api/product/myproduct')
        .set('Authorization', `Bearer ${invalidToken}`)
        const { code, status } = res.body
        expect(code).toBe(400);
        expect(status).toBe("invalid signature");
    });
});

//PRODUCT
describe('PUT PRODUCT',()=>{
    it('Should be publish product', async () => {
        const res = await request(app).put('/api/product/publish/id/1').send({
            'published': true
        })
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body;
        expect(code).toBe(200);
        expect(data).toBe("produk berhasil diupdate");
    });
    it('Should not be publish because its not boolean', async () => {
        const res = await request(app).put('/api/product/publish/id/1').send({
            'published': "true"
        })
        .set('Authorization', `Bearer ${newToken}`)
        const { code, status, message } = res.body;
        expect(code).toBe(400);
        expect(status).toBe("Bad Request");
        expect(message).toBe('published harus bertipe boolean');
    });
    it('Should not be publish because product not found', async () => {
        const res = await request(app).put('/api/product/publish/id/10').send({
            'published': true
        })
        .set('Authorization', `Bearer ${newToken}`)
        const { code, status, message } = res.body;
        expect(code).toBe(404);
        expect(status).toBe("Not Found");
        expect(message).toBe('product not found');
    });
});

describe('POST PRODUCT',()=>{
    it('Should be create product', async () => {
        const res = await request(app).post('/api/product').send({
            'nama': 'Xiomi mi band 3',
            'harga': 500000,
            'kategori_id': 4,
            'deskripsi': 'Xiomi mi bandnya gezz'
        })
        .set('Authorization', `Bearer ${newToken}`)
        const { code } = res.body;
        expect(code).toBe(201);
    },5 * 30000);
});

describe('DELETE PRODUCT',()=>{
    it('Should be delete product', async () => {
        const res = await request(app).delete('/api/product/id/13')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body;
        expect(code).toBe(200);
        expect(data).toBe("produk berhasil didelete");
    });
    it('Should not be delete product', async () => {
        const res = await request(app).delete('/api/product/id/13')
        .set('Authorization', `Bearer ${expiredToken}`)
        const { code, status } = res.body;
        expect(code).toBe(400);
        expect(status).toBe("jwt expired");
    });
    it('Should not found product', async () => {
        const res = await request(app).delete('/api/product/id/13')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, status, message } = res.body;
        expect(code).toBe(404);
        expect(status).toBe("Not Found");
        expect(message).toBe("product not found");
    });
});

//TRANSACTION
describe('TRANSACTION',()=>{
    it('Should be bid for product', async () => {
        const res = await request(app).post('/api/transaction/id/2').send({
            'harga': '10000'
        })
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body;
        expect(code).toBe(200);
        expect(data.id_produk).toBe("2");
        expect(data.nama_produk).toBe("laptop");
        expect(data.harga_produk).toBe(3000000);
        expect(data.harga_tawar).toBe("10000");
    });
    it('Should be failed for bid', async () => {
        const res = await request(app).post('/api/transaction/id/2').send({
            'harga': '10000'
        })
        .set('Authorization', `Bearer ${newToken}`)
        const { code, status, message } = res.body;
        expect(code).toBe(400);
        expect(status).toBe("Bad Request");
        expect(message).toBe("anda hanya dapat membuat satu penawaran pada satu product");
    });
    it('Should be bid for product', async () => {
        const res = await request(app).post('/api/transaction/id/2').send({
            'harga': '10000'
        })
        .set('Authorization', `Bearer ${newToken}`)
        const { code, status } = res.body;
        expect(code).toBe(400);
        expect(status).toBe("Hanya Pembeli yang dapat Menawar Barang anda");
    });
});

describe('GET TRANSACTION',()=>{
    it('Should be get transaction by id', async () => {
        const res = await request(app).get('/api/transaction/id/1')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body;
        expect(code).toBe(200);
        expect(data.penawar).toBe('aziz');
        expect(data.kota).toBe('MANOKWARI SELATAN');
    })
    it('Should not get transaction by id', async () => {
    const res = await request(app).get('/api/transaction/id/2')
    .set('Authorization', `Bearer ${newToken}`)
        const { code, status, message } = res.body;
        expect(code).toBe(404);
        expect(status).toBe('not found');
        expect(message).toBe('tidak terdapat penawaran dari user terhadap produk anda');
    })
    it('Should be get transaction by current user', async () => {
        const res = await request(app).get('/api/transaction/myproduct')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body;
        expect(code).toBe(200);
        expect(data[0].id).toBe(9);
        expect(data[0].harga_tawar).toBe('10000');
    })
    it('Should not get transaction by current user', async () => {
        const res = await request(app).get('/api/transaction/myproduct')
        .set('Authorization', `Bearer ${expiredToken}`)
        const { code, status } = res.body;
        expect(code).toBe(400);
        expect(status).toBe('jwt expired');
    })
    it('Should be get transaction by buyer id', async () => {
        const res = await request(app).get('/api/transaction/id/1')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body;
        expect(code).toBe(200);
        expect(data.penawar).toBe('aziz');
        expect(data.kota).toBe('MANOKWARI SELATAN');
    })
    it('Should not get transaction by buyer id', async () => {
        const res = await request(app).get('/api/transaction/id/2')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, status, message } = res.body;
        expect(code).toBe(404);
        expect(status).toBe('not found');
        expect(message).toBe('tidak terdapat penawaran dari user terhadap produk anda');
    })
    it('Should be get sold product by current user', async () => {
        const res = await request(app).get('/api/transaction/myproduct/sold')
        .set('Authorization', `Bearer ${newToken}`)
        const { code } = res.body;
        expect(code).toBe(200);
    })
    it('Should be get sold product by current user', async () => {
        const res = await request(app).get('/api/transaction/myproduct/sold')
        .set('Authorization', `Bearer ${expiredToken}`)
        const { code, status } = res.body;
        expect(code).toBe(400);
        expect(status).toBe('jwt expired');
    })
});

describe('PUT TRANSACTION', ()=>{
    it('Should be accept the bid', async () => {
        const res = await request(app).put('/api/transaction/terima/id/11').send({
            'opsi': true
        })
        //change opsi to false if u wanna reject the bid
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body;
        expect(code).toBe(200);
        expect(data).toBe('Penawaran berhasil diterima');
    })
    it('Should not accept the bid', async () => {
        const res = await request(app).put('/api/transaction/terima/id/11').send({
            'opsi': true
        })
        //change opsi to false if u wanna reject the bid
        .set('Authorization', `Bearer ${expiredToken}`)
        const { code, status } = res.body;
        expect(code).toBe(400);
        expect(status).toBe('jwt expired');
    })
    it('Should update bid status by id transaction', async () => {
        const res = await request(app).put('/api/transaction/id/11').send({
            'opsi': true
        })
        //change opsi to false if u wanna reject the bid
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body;
        expect(code).toBe(200);
        expect(data).toBe('Berhasil di ganti dan menghapus penawaran lain');
    })
    it('Should not update bid status by id transaction', async () => {
        const res = await request(app).put('/api/transaction/id/11').send({
            'opsi': true
        })
        //change opsi to false if u wanna reject the bid
        .set('Authorization', `Bearer ${expiredToken}`)
        const { code, status } = res.body;
        expect(code).toBe(400);
        expect(status).toBe('jwt expired');
    })
})

describe('DELETE TRANSACTION', ()=>{
    it('Should be accept the bid', async () => {
        const res = await request(app).delete('/api/transaction/id/12')
        .set('Authorization', `Bearer ${newToken}`)
        const { code, data } = res.body;
        expect(code).toBe(200);
        expect(data).toBe('berhasil di hapus');
    })
    it('Should be accept the bid', async () => {
        const res = await request(app).delete('/api/transaction/id/12')
        .set('Authorization', `Bearer ${expiredToken}`)
        const { code, status } = res.body;
        expect(code).toBe(400);
        expect(status).toBe('jwt expired');
    })
})