const Response = require('../helpers/response')
module.exports = (req,res,next)=>{
    const response = new Response(res);

    const {name,city_id,address,phone_number} = req.body;

    if(!name) return response.Fail(response.BadRequest,'Bad Request','name tidak boleh kosong')
    if(!city_id) return response.Fail(response.BadRequest,'Bad Request','city_id tidak boleh kosong')
    if(!address) return response.Fail(response.BadRequest,'Bad Request','address tidak boleh kosong')
    if(!phone_number) return response.Fail(response.BadRequest,'Bad Request','phone_number tidak boleh kosong')

    const numberFormat = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
    const isValidPhoneNumber = phone_number.match(numberFormat)

    const validName = /^[a-zA-Z ]*$/
    const isValidName = name.match(validName)
    if(!isValidName) return response.Fail(response.BadRequest,'Bad Request','name hanya boleh mengandung alphabet')

    if(!isValidPhoneNumber) return response.Fail(response.BadRequest,'Bad Request','phone_number tidak valid')
 
    if(name.length > 75) return response.Fail(response.BadRequest,'Bad Request','name maksimal 75 karakter')
    if(address.length > 400) return response.Fail(response.BadRequest,'Bad Request','address maksimal 400 karakter')

    next();
}