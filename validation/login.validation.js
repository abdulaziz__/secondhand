const Response = require('../helpers/response')
module.exports = (req,res,next)=>{
    const response = new Response(res);
    const {email,password} = req.body;

    if(!email) return response.Fail(response.BadRequest,'Bad Request','email tidak boleh kosong')
    if(!password) return response.Fail(response.BadRequest,'Bad Request','password tidak boleh kosong')

    const validEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const isValidEmail = email.match(validEmail)
    if(!isValidEmail) return response.Fail(response.BadRequest,'Bad Request','email tidak valid')
   
    if(password.length < 6) return response.Fail(response.BadRequest,'Bad Request','password minimal 6 karakter')

    const hasSpace = !password.match(/^\S*$/) && !confirmPassword.match(/^\S*$/)
    if(hasSpace) return response.Fail(response.BadRequest,'Bad Request','password tidak boleh mengandung spasi')
 
    next();
}