const Response = require('../helpers/response')
module.exports = (req,res,next)=>{
    const response = new Response(res);
    const {opsi} = req.body;

    if(typeof(opsi) !== "boolean") return response.Fail(response.BadRequest,'Bad Request','opsi harus bertipe boolean')
      
    next();
}