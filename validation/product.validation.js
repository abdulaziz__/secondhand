const Response = require('../helpers/response')
module.exports = (req,res,next)=>{
    const response = new Response(res);
    const {nama,harga,kategori_id,deskripsi} = req.body;

    if(!nama) return response.Fail(response.BadRequest,'Bad Request','nama tidak boleh kosong')
    if(!harga) return response.Fail(response.BadRequest,'Bad Request','harga tidak boleh kosong')
    if(!kategori_id) return response.Fail(response.BadRequest,'Bad Request','kategori_id tidak boleh kosong')
    if(!deskripsi) return response.Fail(response.BadRequest,'Bad Request','deskripsi tidak boleh kosong')

    if(nama.length > 75) return response.Fail(response.BadRequest,'Bad Request','nama maksimal 75 karakter')
    
    if(harga < 0) return response.Fail(response.BadRequest,'Bad Request','harga harus bernilai positif')
    
    if(deskripsi.length > 1000) return response.Fail(response.BadRequest,'Bad Request','deskripsi maksimal 1000 karakter')
    
    next();
}