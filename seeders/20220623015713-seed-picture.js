'use strict';
const pictureData = require('../masterData/picture.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const picture = pictureData.map((eachPicture) => {
      eachPicture.createdAt = new Date();
      eachPicture.updatedAt = new Date();
      return eachPicture;
    });

   await queryInterface.bulkInsert('pictures', picture);
  },

  async down (queryInterface, Sequelize) {
     await queryInterface.bulkDelete('pictures', null, { truncate: true, restartIdentity: true }) 
  }
};
