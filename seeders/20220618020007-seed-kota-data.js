"use strict";
const Kota = require("../masterData/city.json");

module.exports = {
  async up(queryInterface, Sequelize) {

    const dataKotaToBeSedded = Kota.map((eachCitiesData) => {
      return {
        nama_kota: eachCitiesData.name,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
    await queryInterface.bulkInsert("kota", dataKotaToBeSedded);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("kota", null, { truncate: true, restartIdentity: true });
  },
};
