'use strict';
const productData = require('../masterData/product.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const product = productData.map((eachproduct) => {
      eachproduct.createdAt = new Date();
      eachproduct.updatedAt = new Date();
      return eachproduct;
    });

   await queryInterface.bulkInsert('products', product);
  },

  async down (queryInterface, Sequelize) {

     await queryInterface.bulkDelete('products', null, { truncate: true, restartIdentity: true }) 
  }
};
