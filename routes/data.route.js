const router = require("express").Router();
const {getAllCity,getAllCategory} = require('../controllers/data.controller')

router.get("/kota", getAllCity);
router.get("/kategori", getAllCategory);

module.exports = router;