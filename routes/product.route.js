const router = require("express").Router();
const {getAll,getByCategory,getById,getByCurrentUser,create,update,deleteById,deleteImageById,publishById} = require('../controllers/product.controller')
const productValidator = require('../validation/product.validation') 
const productSanitize = require('../sanitize/product.sanitize')
const uploadPhoto = require('../misc/multer')

const authHeader = require("../misc/auth.header");

router.get("/", getAll);
router.get("/id/:id", authHeader,getById);
router.get("/category/:category", getByCategory);
router.get("/myproduct",authHeader,getByCurrentUser);

router.post('/',authHeader,uploadPhoto.array('product_pict',4),productValidator,productSanitize,create)
router.put('/id/:id',authHeader,uploadPhoto.array('product_pict',4),productValidator,productSanitize,update)
router.put('/publish/id/:id',authHeader,publishById)
router.delete('/id/:id',authHeader,deleteById)

router.delete('/images/id/:id',authHeader,deleteImageById)

module.exports = router;