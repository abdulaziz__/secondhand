const router = require("express").Router();
const { getProfile, editProfile } = require("../controllers/detailProfile.controller");
const uploadPhoto = require('../misc/multer')
const profileValidator = require('../validation/profile.validation')
const cloudinary = require('../misc/cloudinary')

const profileSanitize = require('../sanitize/profile.sanitize')

router.get("/", getProfile);
router.put("/", uploadPhoto.single('profile_pict'),cloudinary.profileUpload,profileValidator,profileSanitize,editProfile);

module.exports = router;

