const router = require("express").Router();
const penawaranController = require("../controllers/penawaran.controller");
const transactionValidator = require('../validation/transaction.validation') 
const statusValidator = require('../validation/status.validation') 

router.post("/id/:id", transactionValidator,penawaranController.createPenawaran);
router.get("/myproduct", penawaranController.getPenawaranByCurrentUser);
router.get("/myproduct/sold", penawaranController.getTerjualByCurrentUser);
router.get("/id/:id", penawaranController.getPenawaranByIdPenawar);
router.get("/transaction-id/:id", penawaranController.getPenawaranById);
router.put("/id/:id", statusValidator,penawaranController.editStatusPenawaran);
router.put("/terima/id/:id", statusValidator,penawaranController.terimaPenawaranById);
router.delete("/id/:id", penawaranController.deletePenawaranById);

module.exports = router;
