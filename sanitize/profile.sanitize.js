module.exports = (req,res,next)=>{
    const {name,address} = req.body;
    req.body.name = name.replace(/\s{2,}/g, ' ').trim() 
    req.body.address = address.replace(/\s{2,}/g, ' ').trim() 
    next();
}