module.exports = (req,res,next)=>{
    const {email,password} = req.body;
    req.body.email = email.replace(/\s{2,}/g, ' ').trim() 
    req.body.password = password.replace(/\s{2,}/g, ' ').trim() 
    next();
}