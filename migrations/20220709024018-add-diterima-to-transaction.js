'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'penawarans', // table name
      'terima', // new field name
      {
        type: Sequelize.BOOLEAN
      }
    )
  },

  async down (queryInterface, Sequelize) {
    queryInterface.removeColumn('penawarans', 'terima')
  }
};
