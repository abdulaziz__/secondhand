'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class picture extends Model {
    
    static associate(models) {
      picture.belongsTo(models.product,{
        foreignKey:'product_id'
      })
    }
  }
  picture.init({
    product_id: DataTypes.INTEGER,
    img_url: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'picture',
  });
  return picture;
};