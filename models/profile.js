"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class profile extends Model {
 
    static associate(models) {
      // define association here
      profile.belongsTo(models.account, {
        foreignKey: "account_id",
      });
      profile.belongsTo(models.kota, {
        foreignKey: "city_id",
      });
    }
  }
  profile.init(
    {
      name: DataTypes.STRING,
      address: DataTypes.STRING,
      phone_number: DataTypes.STRING,
      account_id: DataTypes.INTEGER,
      city_id: DataTypes.INTEGER,
      img_url: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "profile",
    }
  );
  return profile;
};
