"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class penawaran extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      penawaran.belongsTo(models.account, {
        foreignKey: "id_pembeli",
        as:"penawar"
      });
      penawaran.belongsTo(models.product, {
        foreignKey: "id_product",
      });
    }
  }
  penawaran.init(
    {
      id_penjual: DataTypes.INTEGER,
      id_product: DataTypes.INTEGER,
      id_pembeli: DataTypes.INTEGER,
      harga_tawar: DataTypes.STRING,
      is_sold: DataTypes.BOOLEAN,
      terima: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "penawaran",
    }
  );
  return penawaran;
};
