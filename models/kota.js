"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class kota extends Model {

    static associate(models) {
      kota.hasMany(models.profile, {
        foreignKey: "city_id",
      });
    }
  }
  kota.init(
    {
      nama_kota: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "kota",
    }
  );
  return kota;
};
