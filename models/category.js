'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class category extends Model {
    static associate(models) {
      category.hasMany(models.product,{
        foreignKey:'kategori_id'
      })
    }
  }
  category.init({
    category: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'category',
  });
  return category;
};